<?php

namespace App\EQP\Repositories;
use App\EquipmentConfig;

class EquipmentConfigRepository extends Repository
{
    protected function getEntityName()
    {
        return 'App\EquipmentConfig';
    }

    protected function createEntityFromJSON($json)
    {
        $entity = new EquipmentConfig();
        $entity->key = $json['key'];
        $entity->value = $json['value'];

        return $entity;
    }

    protected function updateEntityFromJSON(&$entity, $json)
    {
        $entity->key = $json['key'];
        $entity->value = $json['value'];
    }
}