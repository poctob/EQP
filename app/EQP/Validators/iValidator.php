<?php

namespace App\EQP\Validators;

interface iValidator
{
    public function validate($entity);
    public function addError($variable, $error);
    public function getErrors();
}