<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\EQP\Repositories\EquipmentConfigRepository;
use App\EQP\Validators\EquipmentConfigValidator;

class ConfigController extends Controller
{
    public function list()
    {
       return view('config');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $repository = new EquipmentConfigRepository();
        return $repository->getAll();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('config');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = new EquipmentConfigValidator();
        
        if( $validator->validate($request->all()))
        {          
             $repository = new EquipmentConfigRepository();
             $entity = $repository->saveFromJSON($request->all());
             if($entity)
             {
                 return $entity;
             }
        }
        else
        {
            return response()->json($validator->getErrors(),412);            
        }

        return response()->json(['Status' => 'Something went wrong'], 500);        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $repository = new EquipmentConfigRepository();
        return $repository->getById($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = new EquipmentConfigValidator();

        if( $validator->validate($request->all()))
        {          
             $repository = new EquipmentConfigRepository();
             if($repository->updateFromJSON($request->all(), $id))
             {
                 return response()->json(['Status' => 'Success'], 200);
             }
        }
        else
        {
            return response()->json($validator->getErrors(),412);            
        }

        return response()->json(['Status' => 'Something went wrong'], 500); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $repository = new EquipmentConfigRepository();

        if($repository->delete($id))
        {
            return response()->json(['Status' => 'Success'], 200);
        }

        return response()->json(['Status' => 'Something went wrong'], 500); 
    }
}
