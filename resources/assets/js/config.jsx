var React = require('react'),
  ReactDOM = require('react-dom'),
  Config = require('./collections/configCollection'),
  NavigationMenu = require('./components/navigationMenu'),
  configCollection = new Config(),
  navItems = new Array(
    {
      eventKey: 'Equipment',
      title: 'Equipment',
      label: 'Equipment'
    },
    {
      eventKey: 'Categories',
      title: 'Categories',
      label: 'Categories'
    },
    {
      eventKey: 'Status',
      title: 'Status',
      label: 'Status'
    },
    {
      eventKey: 'General',
      title: 'General',
      label: 'General',
      model: configCollection
    }),
  selectedNavItem = 'General';

function onMenuChange(selection) {
  selectedNavItem = selection;
};

ReactDOM.render(
  <NavigationMenu
    navItems = {navItems}
    defaultNavItem = 'General'
    onChangeCallBack = {onMenuChange}
    />,
  document.getElementById('menuContainer')
);


