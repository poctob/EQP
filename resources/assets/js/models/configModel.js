var Backbone = require('backbone'),
  Config;

Config = Backbone.Model.extend({

  getProperties: function () {
    return new Array(this.get('key'), this.get('value'));
  }
});

module.exports = Config;