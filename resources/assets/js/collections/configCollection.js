var Backbone = require('backbone'),
  Config = require('../models/configModel'),
  ConfigCollection;

ConfigCollection = Backbone.Collection.extend({
  model: Config,

  url: '/eqp/public/config',

  headers: new Array('Key', 'Value'),

  layoutItems: new Array(
    {
      key: 'key',
      editLabel: 'Enter a key for item:',
      type: 'text',
      defaultValue: '',
      placeholderText: 'Enter new key'
    },

    {
      key: 'value',
      editLabel: 'Enter a value for item:',
      type: 'text',
      defaultValue: '',
      placeholderText: 'Enter new value'
    }
  )
});

module.exports = ConfigCollection;