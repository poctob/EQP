var React = require('react'),
    ButtonToolbar = require('react-bootstrap/lib/ButtonToolbar'),
    Button = require('react-bootstrap/lib/Button'),
    Modal = require('react-bootstrap/lib/Modal');

var ConfirmationDialog = React.createClass({

  getInitialState: function(){
    return {
      confirmed: false         
    }
  },

  close() {
    if( this.props.onClose !== undefined) {
        this.props.onClose();
    }  
  },

  open() {
    if( this.props.onOpen !== undefined) {
        this.props.onOpen();
     }       
  },

  setYes: function() {    
    this.setState({ confirmed: true});
    if( this.props.onConfirm !== undefined) {
        this.props.onConfirm();
    } 
  },

  setNo: function(e) {
    this.setState({ confirmed: false});
    if( this.props.onDeny !== undefined) {
        this.props.onDeny();
    } 
  },

  render: function() {

    return(
      <div>
        <Modal show={this.props.show} onHide={this.close}>
            <Modal.Header closeButton>
              <Modal.Title>{this.props.title}</Modal.Title>
            </Modal.Header>
           
              <Modal.Body>
               {this.props.text}
              </Modal.Body>
              <Modal.Footer>
                <ButtonToolbar>
                  <Button bsStyle = 'success' onClick={this.setYes}>{this.props.yesButtonText}</Button>
                  <Button bsStyle = 'danger' onClick={this.setNo}>{this.props.noButtonText}</Button>
                </ButtonToolbar>
              </Modal.Footer>            
          </Modal>
      </div>
    );
  }
});

module.exports = ConfirmationDialog;