var React = require('react'),
  Modal = require('react-bootstrap/lib/Modal'),
  Button = require('react-bootstrap/lib/Button'),
  Spinner = require('react-spinkit'),
  ConfigTable = require('./configTable'),
  ConfirmationDialog = require('./confirmationDialog'),
  EditDialog = require('./editDialog');

var ConfigContainer = React.createClass({
  getInitialState: function () {
    return {
      title: 'General Configuration',
      newButtonText: 'Add',
      showAddModal: false,
      addModalTitle: 'Add new item',
      confirmationTitle: 'Confirm Delete',
      confirmationText: 'Are You Sure You Want to Delete This Item?',
      showConfirmationDialog: false,
      deleteYesButtonText: 'Yes, Do It Now!',
      deleteNoButtonText: 'No, I Changed My Mind...',
      deleteCandidate: null,
      editCandidate: null,
      loading: false
    }
  },

  onAddItemFormSubmit: function (changedProperties) {
    this.onAddModalClose();
    this.setState({ loading: true });

    if (this.state.editCandidate !== null) {
      this.state.editCandidate.save(
        changedProperties,
        { success: this.onSync });
      this.setState({ editCandidate: null });
    } else {
      this.props.data.create(changedProperties, {
        wait: true,
        success: this.onSync
      });
    }
  },

  onSync: function () {
    this.setState({ loading: false });
  },

  handleNew: function (model) {
    this.setState({ editCandidate: null });
    this.openAddModal();
  },

  handleDelete: function (model) {
    this.setState({ deleteCandidate: model });
    this.openDeleteConfirm();
  },

  handleEdit: function (model) {
    this.setState({ editCandidate: model });
    this.openAddModal();
  },

  onAddModalClose: function () {
    this.setState({ showAddModal: false });
  },

  openAddModal: function () {
    this.setState({ showAddModal: true });
  },

  onDeleteConfirmClose: function () {
    this.setState({ showConfirmationDialog: false });
  },

  openDeleteConfirm: function () {
    this.setState({ showConfirmationDialog: true });
  },

  onDeleteConfirm: function () {
    this.onDeleteConfirmClose();

    if (this.state.deleteCandidate !== null) {
      this.state.deleteCandidate.destroy({ success: this.onSync });
      this.setState({ deleteCandidate: null });
    }
  },

  componentDidMount: function () {
    this.syncData();
  },

  syncData: function () {
    this.props.data.on('sync', this.onSync);
    this.props.data.fetch();
  },

  render: function () {
    return (

      <div>
        <div className = {this.state.loading ? '' : 'hidden'}>
          <Spinner spinnerName='three-bounce' />
        </div>

        <div className = {this.state.loading ? 'disabledContainer' : ''}>

          <div className='page-header'>{this.state.title}</div>
          <Button bsStyle="primary" onClick={this.handleNew}>
            <span className="glyphicon glyphicon-plus" aria-hidden="true"></span> {this.state.newButtonText}
          </Button>

          <ConfigTable
            data = {this.props.data !== undefined ? this.props.data : null}
            handleDelete = {this.handleDelete}
            handleEdit = {this.handleEdit}
            />

          <ConfirmationDialog
            title={this.state.confirmationTitle}
            text={this.state.confirmationText}
            show={this.state.showConfirmationDialog}
            onClose = {this.onDeleteConfirmClose}
            onConfirm = {this.onDeleteConfirm}
            onDeny = {this.onDeleteConfirmClose}
            yesButtonText = {this.state.deleteYesButtonText}
            noButtonText = {this.state.deleteNoButtonText}
            />

          <EditDialog
            showAddModal = {this.state.showAddModal}
            title = {this.state.addModalTitle}
            onClose ={this.onAddModalClose}
            onSubmit = {this.onAddItemFormSubmit}
            item = {this.state.editCandidate}
            layoutItems = {this.props.data.layoutItems}
            />

        </div>
      </div>
    );
  }

});

module.exports = ConfigContainer;