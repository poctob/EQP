var React = require('react'),
    Nav = require('react-bootstrap/lib/Nav'),
    ConfigContainer = require('./configContainer'),
    NavItem = require('react-bootstrap/lib/NavItem');

var NavigationMenu = React.createClass({

    getInitialState: function () {
        return {
            activeKey: null
        }
    },

    handleSelect: function (selectedKey) {
        this.setState({ activeKey: selectedKey });
    },

    getCurrentData: function (selectedKey) {
        var navItems = this.props.navItems;

        if (navItems !== undefined && navItems !== null) {
            for (var i = 0; i < navItems.length; i++) {
                if (navItems[i].eventKey === selectedKey) {
                    return navItems[i].model;
                }
            }
        }

        return null;
    },

    componentWillReceiveProps: function (nextProps) {
        if (nextProps.tabContent !== null) {
            this.tabContent = nextProps.tabContent;
        }
    },

    render: function () {
        var navItems = this.props.navItems,
            defaultSelection = this.props.defaultNavItem;

        if (this.state.activeKey !== null) {
            defaultSelection = this.state.activeKey;
        }

        return (
            <div>
                <Nav bsStyle="pills" activeKey={defaultSelection} onSelect={this.handleSelect}>
                    {navItems.map(function (item) {
                        return (
                            <NavItem key={item.eventKey} eventKey={item.eventKey} title={item.title}>{item.label}</NavItem>
                        );
                    }) }
                </Nav>
                <ConfigContainer data= {this.getCurrentData(defaultSelection) } />;
            </div>
        );
    }
});

module.exports = NavigationMenu;