var React = require('react'),
    Table = require('react-bootstrap/lib/Table'),
    ButtonToolbar = require('react-bootstrap/lib/ButtonToolbar'),
    Button = require('react-bootstrap/lib/Button');

var ConfigTable = React.createClass({
    render: function () {
        return (
            <Table striped>
                <ConfigTable.Header
                    headers = {this.props.data !== null ? this.props.data.headers : null}
                    />
                <ConfigTable.List {...this.props}/>
            </Table>
        );
    }
});

ConfigTable.Header = React.createClass({
    render: function () {
        var headers = new Array(),
            key = 0;

        if (this.props.headers !== undefined && this.props.headers !== null) {
            headers = this.props.headers;
        }

        return (
            <thead>
                <tr>

                    {headers.map(function (item) {
                        return (
                            <ConfigTable.Header.Th
                                key = { key++ }
                                item = {item} />
                        );
                    }) }

                </tr>
            </thead>
        );
    }
});

ConfigTable.Header.Th = React.createClass({
    render: function () {
        return (
            <th>{this.props.item}</th>
        );
    }
});

ConfigTable.List = React.createClass({
    render: function () {
        var rows = [];

        if (this.props.data !== null) {
            this.props.data.models.forEach(function (item) {
                rows.push(<ConfigTable.Row
                    item={item}
                    key={item.cid}
                    handleDelete={this.props.handleDelete}
                    handleEdit={this.props.handleEdit}
                    />);
            }, this);
        }

        return (
            <tbody>{rows}</tbody>
        );
    }
});

ConfigTable.Row = React.createClass({
    getInitialState: function () {
        return {
            editButtonText: 'Edit',
            deleteButtonText: 'Delete'
        }
    },

    deleteItem: function () {
        if (this.props.handleDelete !== undefined) {
            this.props.handleDelete(this.props.item);
        }
    },

    editItem: function () {
        if (this.props.handleEdit !== undefined) {
            this.props.handleEdit(this.props.item);
        }
    },

    render: function () {
        var itemProperties = this.props.item.getProperties(),
            key = 0;

        return (
            <tr>
                {itemProperties.map(function (item) {
                    return (
                        <ConfigTable.Row.Td
                            key = { key++ }
                            item = {item} />
                    );
                }) }
                <td>
                    <ButtonToolbar>
                        <Button bsStyle="warning" onClick={this.editItem}>
                            <span className="glyphicon glyphicon-pencil" aria-hidden="true"></span> {this.state.editButtonText}
                        </Button>
                        <Button bsStyle="danger" onClick={this.deleteItem}>
                            <span className="glyphicon glyphicon-remove" aria-hidden="true"></span> {this.state.deleteButtonText}
                        </Button>
                    </ButtonToolbar>
                </td>
            </tr>
        );
    }
});

ConfigTable.Row.Td = React.createClass({
    render: function () {
        return (
            <td>{this.props.item}</td>
        );
    }
});

module.exports = ConfigTable;