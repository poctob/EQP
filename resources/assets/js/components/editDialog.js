var React = require('react'),
  ButtonToolbar = require('react-bootstrap/lib/ButtonToolbar'),
  Button = require('react-bootstrap/lib/Button'),
  Modal = require('react-bootstrap/lib/Modal'),
  FormGroup = require('react-bootstrap/lib/FormGroup'),
  FormControl = require('react-bootstrap/lib/FormControl'),
  ControlLabel = require('react-bootstrap/lib/ControlLabel');

var EditDialog = React.createClass({

  getInitialState: function () {
    return {
      model: null,
      changedValues: {},
      editSaveButtonText: 'Save',
      editCancelButtonText: 'Cancel'
    }
  },

  close() {
    if (this.props.onClose !== undefined) {
      this.props.onClose();
    }
  },

  open() {
    if (this.props.onOpen !== undefined) {
      this.props.onOpen();
    }
  },

  submitForm: function (e) {
    e.preventDefault();

    if (this.props.onSubmit !== undefined) {
      this.props.onSubmit(this.state.changedValues);
    }

    this.setState({ changedValues: {} });
  },

  componentWillReceiveProps: function (nextProps) {
    var model = nextProps.item;
    if (model !== null) {
      this.setState({ model: model });
    } else {
      this.setState({ model: null });
    }
  },

  handleItemChange: function (e) {
    this.state.changedValues[e.target.id] = e.target.value;
  },

  render: function () {
    var layoutItems = new Array();

    if (this.props.layoutItems !== undefined && this.props.layoutItems !== null) {
      layoutItems = this.props.layoutItems;
    }

    return (
      <div>
        <Modal show={this.props.showAddModal} onHide={this.close}>
          <Modal.Header closeButton>
            <Modal.Title>{this.props.title}</Modal.Title>
          </Modal.Header>
          <form onSubmit={this.submitForm}>
            <Modal.Body>

              {layoutItems.map(function (item) {
                return (
                  <FormGroup
                    key = {item.key}
                    controlId={item.key}>
                    <ControlLabel>{item.editLabel}</ControlLabel>
                    <FormControl
                      type= {item.type}
                      defaultValue={this.state.model !== null ? this.state.model.get(item.key) : item.defaultValue}
                      placeholder={item.placeholderText}
                      onChange={this.handleItemChange}
                      />
                  </FormGroup>
                );
              }, this) }

            </Modal.Body>
            <EditDialog.Footer
              saveButtonText = {this.state.editSaveButtonText}
              cancelButtonText = {this.state.editCancelButtonText}
              onCancel = {this.close}
              />
          </form>
        </Modal>
      </div>
    );
  }
});

EditDialog.Footer = React.createClass({

  cancel() {
    if (this.props.onCancel !== undefined) {
      this.props.onCancel();
    }
  },

  render: function () {

    return (
      <Modal.Footer>
        <ButtonToolbar>
          <Button bsStyle = 'success' type='submit'>{this.props.saveButtonText}</Button>
          <Button bsStyle = 'danger' onClick={this.cancel}>{this.props.cancelButtonText}</Button>
        </ButtonToolbar>
      </Modal.Footer>
    );
  }
});

module.exports = EditDialog;